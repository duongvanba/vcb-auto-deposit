export async function sleep(sec: number){
    await new Promise(s => setTimeout(s, sec*1000)) 
}

export async function waitFor(condition: (...args: any[]) => boolean, duration?: number, ping_delay?: number){
    await new Promise(async s => {
        duration && setTimeout(s, duration*1000)
        while(true){
            if(await condition()){
                s()
                break;
            }
            await sleep(ping_delay | 2)
        }
    })
}