export type Transaction = {
    date: Date
    code: string
    description: string
    amount: number
}

export type AccountAndTransaction = { account: string, transaction: Transaction }
