import { Transaction, AccountAndTransaction } from "./types";

export interface EBank {
    list_accounts: () => Promise<string[]>
    list_transactions: (account: string, from?, to?: Date) => Promise<Transaction[]>
    on_new_transactions: (isContinue: () => boolean, hander: (transactions: AccountAndTransaction[]) => any) => any
}