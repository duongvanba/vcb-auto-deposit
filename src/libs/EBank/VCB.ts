import { RequestPromiseAPI, defaults } from 'request-promise'
import { sleep } from '../sleep'
import { Transaction } from './types'
import { EBank } from './EBank'
import { writeFileSync } from 'fs'

export const get_time_string = (d: Date) => `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`



export class VCB implements EBank {
    private client: RequestPromiseAPI
    private __RequestVerificationToken: string
    private TokenData: string
    private scaned_transcations: Set<string> = new Set()

    constructor(private cookie: string) {
        this.client = defaults({
            baseUrl: `https://www.vietcombank.com.vn/ibanking2020/api`,
            headers: { 'User-Agent': '@AutoCode', cookie },
            followAllRedirects: true,
            followOriginalHttpMethod: true,
            followRedirect: true
        })
    }

    async on_new_transactions(
        isContinue: () => boolean,
        hander: (transactions: Array<{ account: string, transaction: Transaction }>) => any
    ) {
        const hash_transaction = (transaction: Transaction) => `${
            transaction.date.getFullYear()
            }/${
            transaction.date.getMonth()
            }/${
            transaction.date.getDate()
            }#${transaction.description}`

        // Scan old transactions
        const accounts = await this.list_accounts()
        for (const account of accounts) {
            const transactions = await this.list_transactions(account)
            transactions.map(t => this.scaned_transcations.add(hash_transaction(t)))
        }

        // Listen new transaction
        setImmediate(async () => {
            while (isContinue()) {
                const new_transactions: Array<{ account: string, transaction: Transaction }> = []
                for (const account of accounts) {
                    const transactions = await this.list_transactions(account)
                    for (const transaction of transactions) {
                        const hash = hash_transaction(transaction)
                        if (this.scaned_transcations.has(hash)) continue
                        this.scaned_transcations.add(hash)
                        new_transactions.push({ account, transaction })
                    }
                }
                if (isContinue()) {
                    new_transactions.length > 0 && await hander(new_transactions)
                    await sleep(30)
                }
            }
        })
    }

    async list_accounts(): Promise<string[]> {
        const html = await this.client.get('thongtintaikhoan/taikhoan/chitiettaikhoan') as string
        const matches = html.match(/<option value=.+?>([0-9]+)/g)
        if (!matches) throw new Error('Cookie error')
        const accounts: string[] = []
        for (const item of matches) {
            accounts.push(item.match(/<option value=.+?>([0-9]+)/)[1])
        }
        return accounts
    }

    async list_transactions(account: string, from: Date = new Date, to: Date = new Date): Promise<Transaction[]> {
        if (!this.__RequestVerificationToken) {
            const html = await this.client.get('thongtintaikhoan/taikhoan/ChiTietTaiKhoan') as string

            this.__RequestVerificationToken = html.match(/RequestVerificationToken.+?value=(.+?)(?:>| )/)[1]

            if (!this.TokenData) {
                const regex = new RegExp(`<option value=(.+?)>${account}`)
                const TaiKhoanTrichNo = html.match(regex)[1]
                const { TokenData } = await this.client.post('ThongTinTaiKhoan/TaiKhoan/GetThongTinChiTiet', {
                    form: { __RequestVerificationToken: this.__RequestVerificationToken, TaiKhoanTrichNo },
                    json: true
                })
                this.TokenData = TokenData
            }
        }

        const form = {
            NgayBatDauText: '',
            NgayKetThucText: '',
            TokenData: this.TokenData,
            __RequestVerificationToken: this.__RequestVerificationToken
        }

        const data = await this.client.post(
            'ThongTinTaiKhoan/TaiKhoan/ChiTietGiaoDich',
            { form, json: true, timeout: 24 * 60 * 60 * 1000 }
        ).catch(e => []) as {
            ChiTietGiaoDich: Array<{
                SoThamChieu: string,
                SoTienGhiNo: string,
                SoTienGhiCo: string,
                MoTa: string,
                NgayGiaoDich: string
            }>
        }

        if (!data.ChiTietGiaoDich) return []

        const list = data.ChiTietGiaoDich.map(el => {
            const amount = el.SoTienGhiNo == '-' ? Number(el.SoTienGhiCo.replace(/,/g, '')) : - Number(el.SoTienGhiNo.replace(/,/g, ''))
            return {
                amount,
                code: el.SoThamChieu,
                date: new Date(el.NgayGiaoDich),
                description: el.MoTa
            } as Transaction
        })
        return list
    }


} 