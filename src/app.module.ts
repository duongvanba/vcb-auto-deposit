import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CronCheck } from './cron-check.service'; 

@Module({
  controllers: [AppController],
  providers: [CronCheck],
})
export class AppModule { }