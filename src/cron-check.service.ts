import { Injectable } from "@nestjs/common";
import { VCB, get_time_string } from "./libs/EBank/VCB";
import { v4 } from 'uuid'
import { Transaction } from "./libs/EBank/types";
import { post } from 'request-promise'

@Injectable()
export class CronCheck {

    private active_scaner: Map<string, string> = new Map()


    async get_status() {
        return [... this.active_scaner.entries()]
    }

    async active(cookie: string, ping_urls: string[], regex: RegExp): Promise<{ accounts: string[] }> {

        const vcb = new VCB(cookie)
        const id = v4()
        const accounts = await vcb.list_accounts()
        accounts.map(account => this.active_scaner.set(account, id))

        const isContinue = () => {
            for (const account of accounts) {
                if (this.active_scaner.get(account) != id) return false
            }
            return true
        }

        const handler = async (transactions: Array<{ account: string, transaction: Transaction }>) => {
            const d = new Date
            for (const { account, transaction } of transactions) {
                const match = transaction.description.toLowerCase().match(new RegExp(regex))
                const data = {
                    account,
                    time: `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`,
                    ...transaction,
                    username: match ? match[1] : ''
                }
                for (const url of ping_urls) {
                    try {
                        await post(url, url.includes('sheety.co') ? { json: { log: data } } : { form: data })
                    } catch (e) {
                        console.log(`Can not post to ${url}`)
                    }
                }
            }
        }

        await vcb.on_new_transactions(isContinue, handler)

        return { accounts }
    }
}


