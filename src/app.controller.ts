import { Controller, Get, Query, Post, Body } from "@nestjs/common";
import { CronCheck } from "./cron-check.service";



@Controller()
export class AppController {

    constructor (private CronCheck: CronCheck) {

    }


    @Get()
    async home() {
        return { error: false, message: 'Server online' }
    }

    @Post('start')
    async start(
        @Body('cookie') cookie: string,
        @Body('regex') regex: string,
        @Body('post_urls') post_urls: string
    ) {
        if (!cookie || cookie == '') return { error: true, message: 'Missing cookie' }
        try {
            const urls = post_urls.split('\n').map(u => u.trim())
            const data = await this.CronCheck.active(cookie, urls, new RegExp(regex))
            return { error: false, message: 'Success installed', data, time: Date.now() }
        } catch (e) {
            console.log(e)
            return { error: true, message: e.message }
        }
    }

    @Get('status')
    async getStatus() {
        const data = await this.CronCheck.get_status()
        return { error: false, data }
    }
}