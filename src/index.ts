import { NestFactory } from '@nestjs/core';
import {
    FastifyAdapter,
    NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { AppModule } from './app.module';
import * as helmet from 'helmet';



async function bootstrap() {
    const app = await NestFactory.create<NestFastifyApplication>(
        AppModule,
        new FastifyAdapter()
    );  
    app.use(helmet())
    app.enableCors()

    await app.listen(process.env.PORT || 8080, '0.0.0.0');
}
bootstrap();