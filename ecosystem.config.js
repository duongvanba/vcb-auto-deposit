module.exports = {
  apps: [{
      name: "cron",
      script: "./dist/index.js",
      env: {
        PORT: 8080,
        TZ: 'Asia/Ho_Chi_Minh',
      }
    },
    {
      name: "home",
      script: "serve",
      env: {
        PM2_SERVE_PATH: 'public',
        PM2_SERVE_PORT: 8888
      }
    }
  ],

}